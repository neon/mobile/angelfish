Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: plasma-angelfish
Source: https://phabricator.kde.org/source/plasma-angelfish/

Files: *
Copyright: 2014, 2015, Sebastian Kügler <sebas@kde.org>
License: GPL-3.0+

Files: autotests/*
Copyright: 2014, Alex Richardson <arichardson.kde@gmail.com>
License: LGPL-2

Files: debian/*
Copyright: 2018, Jonah Brüchert <jbb@kaidan.im>
License: GPL-3.0+

Files: src/*
Copyright: 2014, 2015, Sebastian Kügler <sebas@kde.org>
License: GPL-2+

Files: src/main.cpp
Copyright: 2019, Jonah Brüchert <jbb.prv@gmx.de>
License: LGPL-2+

Files: src/regex-weburl/*
Copyright: 2010-2018, Diego Perini (http:www.iport.it)
License: Expat

Files: src/urlfilterproxymodel.cpp
  src/urlfilterproxymodel.h
Copyright: 2019, Simon Schmeisser <s.schmeisser@gmx.net>
License: GPL-2+

License: Expat
 This software is Copyright (c) 2019 by foo.
 .
 This is free software, licensed under:
 .
   The MIT (X11) License
 .
 The MIT License
 .
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall
 be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published by the
 Free Software Foundation; version 2 of the License.
 .
 On Debian systems, the complete text of version 2 of the GNU Library
 General Public License can be found in `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published by the
 Free Software Foundation; version 2 of the License, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU Library
 General Public License can be found in `/usr/share/common-licenses/LGPL-2'.
